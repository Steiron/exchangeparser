using System;
using System.IO;
using System.Net;
using Microsoft.Extensions.Configuration;

namespace ExchangeParser.Parser
{
    public class ParserHelper
    {
        public string SavePath { get; private set; }
        private readonly int _currentYear = DateTime.Now.Year;

        public ParserHelper(IConfiguration configuration)
        {
            SetSavePath(configuration);
        }

        private void SetSavePath(IConfiguration configuration)
        {
            string saveDir = configuration.GetSection("SavePath").Value ?? "C:\temp_download";
            if (!Directory.Exists(saveDir))
            {
                Directory.CreateDirectory(saveDir);
            }

            string fileName = $"exchange-{_currentYear}.txt";
            SavePath = Path.Combine(saveDir, fileName);
        }

        public void DownloadFile()
        {
            string urlPath =
                $"https://www.cnb.cz/en/financial_markets/foreign_exchange_market/exchange_rate_fixing/year.txt?year={_currentYear}";
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadFile(urlPath, SavePath);
            }
        }
    }
}