using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Interfaces.Services;
using ExchangeParser.Table;
using Microsoft.Extensions.DependencyInjection;

namespace ExchangeParser.Parser
{
    public class FileParser
    {
        private readonly IExchangeService _exchangeService;

        public FileParser(IServiceProvider serviceProvider)
        {
            _exchangeService = serviceProvider.GetService<IExchangeService>();
        }

        public async Task<TableHelper> Parse(string pathToFile)
        {
            TableHelper table = new TableHelper();

            var lines = File.ReadLines(pathToFile);

            int tableColumn = 0;
            int tableRow = 0;
            DateTime? lastParseDay = await _exchangeService.GetDateInfo();

            foreach (string line in lines)
            {
                var columns = line.Split("|");
                string date = "";

                foreach (string column in columns)
                {
                    if (tableRow == 0)
                    {
                        table.AddCurrency(column, tableColumn);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(date))
                        {
                            date = column;
                        }
                        
                        DateTime curDate = DateTime.Parse(date);
                        if (lastParseDay != null && curDate < lastParseDay) continue;

                        table.AddBody(column, date, tableColumn);
                    }

                    tableColumn++;
                }

                tableColumn = 0;
                tableRow++;
            }

            return table;
        }
    }
}