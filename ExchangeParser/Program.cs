﻿using ExchangeParser.Converter;
using ExchangeParser.Parser;
using ExchangeParser.Table;

namespace ExchangeParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Startup startup = new Startup();
            ExchangeDataConverter converter = new ExchangeDataConverter(startup.ServiceProvider);
            ParserHelper parserHelper = new ParserHelper(startup.Configuration);
            FileParser fileParser = new FileParser(startup.ServiceProvider);


            parserHelper.DownloadFile();
            TableHelper table = fileParser.Parse(parserHelper.SavePath).Result;
            converter.Convert(table).Wait();
        }
    }
}