using System.Collections.Generic;

namespace ExchangeParser.Table
{
    public class TableCurrency
    {
        public string Name { get; }
        public int Count { get; }
        public int ColumnNumber { get; }

        public IReadOnlyCollection<TableDatePrice> TableDatePrices => _datePrices;
        private List<TableDatePrice> _datePrices = new List<TableDatePrice>();


        public TableCurrency(string fullName, int columnNumber)
        {
            string[] splitName = fullName.Split(" ");
            Name = splitName[1];
            Count = int.Parse(splitName[0]);

            ColumnNumber = columnNumber;
        }

        public void AddDatePrice(string value, string date)
        {
            TableDatePrice tableDatePrice = new TableDatePrice(value, date);
            _datePrices.Add(tableDatePrice);
        }
    }
}