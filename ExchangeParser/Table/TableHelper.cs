using System.Collections.Generic;
using System.Linq;

namespace ExchangeParser.Table
{
    public class TableHelper
    {
        public IReadOnlyCollection<TableCurrency> TableCurrencies => _tableCurrencies;

        private List<TableCurrency> _tableCurrencies = new List<TableCurrency>();


        private bool CheckFirstColumn(int column) => column == 0;

        public void AddCurrency(string name, int column)
        {
            if (CheckFirstColumn(column)) return;

            TableCurrency currency = new TableCurrency(name, column);
            _tableCurrencies.Add(currency);
        }

        public void AddBody(string value, string date, int column)
        {
            if (CheckFirstColumn(column)) return;

            TableCurrency curCurrency = _tableCurrencies.First(h => h.ColumnNumber == column);
            curCurrency.AddDatePrice(value, date);
        }
    }
}