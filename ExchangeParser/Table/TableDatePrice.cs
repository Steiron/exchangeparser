using System;
using System.Globalization;

namespace ExchangeParser.Table
{
    public class TableDatePrice
    {
        public DateTime Date { get; }
        public decimal Price { get; }


        public TableDatePrice(string value, string date)
        {
            Date = DateTime.Parse(date);
            Price = Convert.ToDecimal(value, CultureInfo.InvariantCulture);
        }
    }
}