using System;
using System.IO;
using DataCore.DataContext;
using DataCore.Interfaces.Repository;
using DataCore.Interfaces.Services;
using DataCore.Repository;
using DataCore.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExchangeParser
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IServiceProvider ServiceProvider { get; private set; }

        public Startup()
        {

            
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);
            Configuration = builder.Build();
            
            ConfigureServices(new ServiceCollection());
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");

            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ExchangeContext>(opt =>
                    opt.UseSqlServer(connectionString));


            ServiceProvider = services
                .AddScoped<IExchangeService, ExchangeService>()
                .AddScoped<IExchangeRepository, ExchangeRepository>()
                .BuildServiceProvider();
        }
    }
}