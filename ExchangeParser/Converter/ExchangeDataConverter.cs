using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Entities;
using DataCore.Interfaces.Services;
using ExchangeParser.Table;
using Microsoft.Extensions.DependencyInjection;

namespace ExchangeParser.Converter
{
    public class ExchangeDataConverter
    {
        private readonly IExchangeService _exchangeService;

        public ExchangeDataConverter(IServiceProvider serviceProvider)
        {
            _exchangeService = serviceProvider.GetService<IExchangeService>();
        }

        public async Task Convert(TableHelper tableHelper)
        {
            var allCurrency = await _exchangeService.GetAllCurrency();
            var lastDays = await _exchangeService.GetLastDays();

            List<DatePrice> datePricesToAdd = new List<DatePrice>();
            List<DatePrice> datePricesToUpdate = new List<DatePrice>();

            foreach (TableCurrency tableCurrency in tableHelper.TableCurrencies)
            {
                Currency currency = allCurrency.FirstOrDefault(c => c.Name == tableCurrency.Name);
                if (currency == null)
                {
                    currency = new Currency(tableCurrency.Name, tableCurrency.Count);
                    currency = await _exchangeService.AddCurrency(currency);
                }

                DatePrice lastDatePrice = lastDays.FirstOrDefault(d => d.CurrencyId == currency.Id);

                foreach (TableDatePrice tableDatePrice in tableCurrency.TableDatePrices)
                {
                    DatePrice datePrice = new DatePrice(currency.Id, tableDatePrice.Date, tableDatePrice.Price);
                    if (CheckActualDate(datePrice.Date, lastDatePrice)) continue;

                    if (CheckUpdatePrice(tableDatePrice.Date, tableDatePrice.Price, lastDatePrice))
                    {
                        datePricesToUpdate.Add(datePrice);
                    }
                    else if (!DatePriceExistInDB(tableDatePrice.Date, lastDatePrice))
                    {
                        datePricesToAdd.Add(datePrice);
                    }
                }
            }

            await _exchangeService.UpdateDatePriceRange(datePricesToUpdate.AsReadOnly());
            await _exchangeService.AddDatePriceRange(datePricesToAdd.AsReadOnly());
        }

        /// <summary>
        /// <para>Осуществляет проверку дат, полученных с парсера и последней даты БД.</para>
        /// <para>Возвращает true если в БД присутствует дата и она больше даты с парсера</para>
        /// </summary>
        /// <param name="date">Дата парсера</param>
        /// <param name="datePrice"></param>
        /// <returns>Возвращает true если в БД присутствует дата и она больше даты с парсера</returns>
        private bool CheckActualDate(DateTime date, DatePrice datePrice)
        {
            return datePrice != null && datePrice.Date > date;
        }

        private bool DatePriceExistInDB(DateTime date, DatePrice datePrice)
        {
            return datePrice != null && datePrice.Date.Equals(date);
        }

        private bool CheckUpdatePrice(DateTime date, decimal price, DatePrice datePrice)
        {
            return datePrice != null && datePrice.Date.Equals(date) && !datePrice.Price.Equals(price);
        }
    }
}