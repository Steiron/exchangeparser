using DataCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataCore.DataContext
{
    public class ExchangeContext : DbContext
    {
        public ExchangeContext(DbContextOptions<ExchangeContext> options) : base(options)
        {
        }


        public DbSet<Currency> Currencies { get; set; }
        public DbSet<DatePrice> DatePrices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var currencyBuilder = modelBuilder.Entity<Currency>();
            currencyBuilder.HasKey(p => p.Id);
            currencyBuilder.Property(p => p.Name).IsRequired();
            currencyBuilder.ToTable("Currency");

            var datePriceBuilder = modelBuilder.Entity<DatePrice>();
            datePriceBuilder.HasKey(p => p.Id);
            datePriceBuilder
                .HasOne(p => p.Currency)
                .WithMany(c => c.DatePrices)
                .HasForeignKey(p => p.CurrencyId)
                .OnDelete(DeleteBehavior.Cascade);
            datePriceBuilder.Property(p => p.Price).HasColumnType("decimal(18,3)");
            datePriceBuilder.ToTable("DatePrice");
        }
    }
}