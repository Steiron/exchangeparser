using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DataCore.DataContext
{
    /// <summary>
    /// Костыль для корректной работы migrations, взято с https://medium.com/oppr/net-core-using-entity-framework-core-in-a-separate-project-e8636f9dc9e5
    /// </summary>
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ExchangeContext>
    {
        public ExchangeContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(Directory.GetCurrentDirectory() + "/../ExchangeParser/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<ExchangeContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new ExchangeContext(builder.Options);
        }
    }
}