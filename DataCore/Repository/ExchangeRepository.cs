using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.DataContext;
using DataCore.Entities;
using DataCore.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Repository
{
    public class ExchangeRepository : IExchangeRepository
    {
        private ExchangeContext _context;

        public ExchangeRepository(ExchangeContext context)
        {
            _context = context;
        }


        public async Task<DateTime?> GetDateInfo()
        {
            var dates = await _context.Currencies
                .Include(c => c.DatePrices)
                .Select(c => c.DatePrices.FirstOrDefault(d => d.CurrencyId == c.Id))
                .OrderByDescending(d => d.Date).ToListAsync();

            if (!dates.Any()) return null;
            return dates.Select(d => d.Date).First();
        }

        public async Task<IReadOnlyCollection<Currency>> GetAllCurrency()
        {
            return await _context.Currencies.ToListAsync();
        }

        public async Task<IReadOnlyCollection<DatePrice>> GetLastDays()
        {
            return await _context.Currencies
                .Include(c => c.DatePrices)
                .Select(c => c.DatePrices.OrderByDescending(d => d.Date).FirstOrDefault())
                .ToListAsync();
        }

        async Task<Currency> IExchangeRepository.AddCurrency(Currency currency)
        {
            await _context.Currencies.AddAsync(currency);
            await _context.SaveChangesAsync();
            return currency;
        }

        public async Task AddDatePriceRange(IReadOnlyCollection<DatePrice> datePrices)
        {
            await _context.DatePrices.AddRangeAsync(datePrices);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateDatePriceRange(IReadOnlyCollection<DatePrice> datePrices)
        {
            _context.DatePrices.UpdateRange(datePrices);
            await _context.SaveChangesAsync();
        }
    }
}