using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Entities;
using DataCore.Interfaces.Repository;
using DataCore.Interfaces.Services;

namespace DataCore.Services
{
    public class ExchangeService : IExchangeService
    {
        private readonly IExchangeRepository _repository;

        public ExchangeService(IExchangeRepository repository)
        {
            _repository = repository;
        }


        public async Task<DateTime?> GetDateInfo()
        {
            return await _repository.GetDateInfo();
        }

        public async Task<IReadOnlyCollection<Currency>> GetAllCurrency()
        {
            return await _repository.GetAllCurrency();
        }

        public async Task<IReadOnlyCollection<DatePrice>> GetLastDays()
        {
            return await _repository.GetLastDays();
        }

        public async Task<Currency> AddCurrency(Currency currency)
        {
            return await _repository.AddCurrency(currency);
        }

        public async Task AddDatePriceRange(IReadOnlyCollection<DatePrice> datePrices)
        {
            if (!datePrices.Any()) return;
            await _repository.AddDatePriceRange(datePrices);
        }

        public async Task UpdateDatePriceRange(IReadOnlyCollection<DatePrice> datePrices)
        {
            if (!datePrices.Any()) return;
            await _repository.UpdateDatePriceRange(datePrices);
        }
    }
}