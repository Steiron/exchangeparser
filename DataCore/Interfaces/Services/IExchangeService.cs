using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataCore.Entities;

namespace DataCore.Interfaces.Services
{
    public interface IExchangeService
    {
        Task<DateTime?> GetDateInfo();
        Task<IReadOnlyCollection<Currency>> GetAllCurrency();
        Task<IReadOnlyCollection<DatePrice>> GetLastDays();
        Task<Currency> AddCurrency(Currency currency);
        Task AddDatePriceRange(IReadOnlyCollection<DatePrice> datePrices);
        Task UpdateDatePriceRange(IReadOnlyCollection<DatePrice> datePrices);
    }
}