using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataCore.Entities;

namespace DataCore.Interfaces.Repository
{
    public interface IExchangeRepository
    {
        Task<DateTime?> GetDateInfo();
        Task<IReadOnlyCollection<Currency>> GetAllCurrency();
        Task<IReadOnlyCollection<DatePrice>> GetLastDays();
        Task<Currency> AddCurrency(Currency currency);
        Task AddDatePriceRange(IReadOnlyCollection<DatePrice> datePrices);
        Task UpdateDatePriceRange(IReadOnlyCollection<DatePrice> datePrices);
    }
}